/*
 * SystemTimer.hpp
 *
 *  Created on: 12.11.2020
 *      Author: Djoum
 */

#ifndef SYSTEMTIMER_HPP_
#define SYSTEMTIMER_HPP_

class SystemTimer
{
public:
   SystemTimer( const unsigned long _timerValue );

   void start( void );
   void pause( void );
   void stop( void );
   unsigned long since( void );

   bool isElapsed( void );

   static void wait( const unsigned long waitingTime );
   static unsigned long now( void );

private:
   unsigned long startTime;
   unsigned long timerValue;
   bool isRunning;
   bool isPause;
};

#endif /* SYSTEMTIMER_HPP_ */
