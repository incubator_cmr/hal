/*
 * GPIOPin.hpp
 *
 *  Created on: 15.11.2020
 *      Author: Djoum
 */

#ifndef GPIOPIN_HPP_
#define GPIOPIN_HPP_

#include "GPIOPort.hpp"

class GPIOPin
{
public:
   GPIOPin( GPIOPort* _port, unsigned char _pin ) : aPin( _pin ), port( _port )
   {
   }

protected:
   unsigned char aPin;
   GPIOPort* port;
};




#endif /* GPIOPIN_HPP_ */
