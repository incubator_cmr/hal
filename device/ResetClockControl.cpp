/*
 * ResetClockControl.cpp
 *
 *  Created on: 15.11.2020
 *      Author: Djoum
 */

#include <ResetClockControl.hpp>

bool ResetClockControl::isPLLReady()
{
   return registers->CR.getBits( (unsigned char)PLLRDY, (unsigned char)PLLRDY ) == 1;
}

void ResetClockControl::enablePLL( bool enable )
{
   if ( enable )
   {
      registers->CR.setBit( (unsigned char)PLLON );
   }
   else
   {
      registers->CR.resetBit( (unsigned char)PLLON );
   }
}

bool ResetClockControl::isPLLEnabled( void )
{
   return registers->CR.getBits( (unsigned char)PLLON, (unsigned char)PLLON ) == 1;
}

void ResetClockControl::enableCSS( bool enable )
{
   if ( enable )
   {
      registers->CR.setBit( (unsigned char)CSSON );
   }
   else
   {
      registers->CR.resetBit( (unsigned char)CSSON );
   }
}

bool ResetClockControl::isCSSEnabled( void )
{
   return registers->CR.getBits( (unsigned char)CSSON, (unsigned char)CSSON ) == 1;
}

void ResetClockControl::bypassExternalClock( bool clockDetector )
{
   if ( clockDetector )
   {
      registers->CR.setBit( (unsigned char)HSEBYP );
   }
   else
   {
      registers->CR.resetBit( (unsigned char)HSEBYP );
   }
}

bool ResetClockControl::isExternalClockBypassed( void )
{
   return registers->CR.getBits( (unsigned char)HSEBYP, (unsigned char)HSEBYP ) == 1;
}

bool ResetClockControl::isHSEReady( void )
{
   return registers->CR.getBits( (unsigned char)HSERDY, (unsigned char)HSERDY ) == 1;
}

void ResetClockControl::enableHSE( bool enable )
{
   if ( enable )
   {
      registers->CR.setBit( (unsigned char)HSEON );
   }
   else
   {
      registers->CR.resetBit( (unsigned char)HSEON );
   }
}

bool ResetClockControl::isHSEEnabled( void )
{
   return registers->CR.getBits( (unsigned char)HSEON, (unsigned char)HSEON ) == 1;
}

unsigned char ResetClockControl::getHSICalibrationValue( void )
{
   return registers->CR.getBits( (unsigned char)HSICAL, (unsigned char)HSICAL + 7 ) == 1;
}

void ResetClockControl::trimHSI( unsigned char trimmingValue )
{
   registers->CR.setBits( trimmingValue, (unsigned char)HSITRIM, (unsigned char)HSITRIM + 4 );
}

unsigned char ResetClockControl::getTrimmingValue( void )
{
   return registers->CR.getBits( (unsigned char)HSITRIM, (unsigned char)HSITRIM + 4 ) == 1;
}

bool ResetClockControl::isHSIReady( void )
{
   return registers->CR.getBits( (unsigned char)HSIRDY, (unsigned char)HSIRDY ) == 1;
}

void ResetClockControl::enableHSI( bool enable )
{
   if ( enable )
   {
      registers->CR.setBit( (unsigned char)HSION );
   }
   else
   {
      registers->CR.resetBit( (unsigned char)HSION );
   }
}

bool ResetClockControl::isHSIEnabled( void )
{
   return registers->CR.getBits( (unsigned char)HSION, (unsigned char)HSION ) == 1;
}


void ResetClockControl::setClockOutput( ClockOutput mco )
{
   registers->CFGR.setBits( (unsigned char)mco, (unsigned char)MCO, (unsigned char)MCO + 3 );
}

void ResetClockControl::enableUSBPrescaler( bool clockIsDivided )
{
   if ( clockIsDivided )
   {
      registers->CFGR.setBit( (unsigned char)USBPRE );
   }
   else
   {
      registers->CFGR.resetBit( (unsigned char)USBPRE );
   }
}

void ResetClockControl::setPLLMultiplicationFactor( PLLInputClock factor )
{
   registers->CFGR.setBits( (unsigned char)factor, (unsigned char)PLLMUL, (unsigned char)PLLMUL + 3 );
}

void ResetClockControl::divideHSEForPLLEntry( bool divide )
{
   if ( divide )
   {
      registers->CFGR.setBit( (unsigned char)PLLXTPRE );
   }
   else
   {
      registers->CFGR.resetBit( (unsigned char)PLLXTPRE );
   }
}

void ResetClockControl::selectPLLSource( bool hsiDividedBy2 )
{
   if ( hsiDividedBy2 )
   {
      registers->CFGR.setBit( (unsigned char)PLLSRC );
   }
   else
   {
      registers->CFGR.resetBit( (unsigned char)PLLSRC );
   }
}

void ResetClockControl::setADCPrescaler( ADCPrescaler prescalerValue )
{
   registers->CFGR.setBits( (unsigned char)prescalerValue, (unsigned char)ADCPRE, (unsigned char)ADCPRE + 3 );
}

void ResetClockControl::setAPB2Prescaler( APBPrescaler prescalerValue )
{
   registers->CFGR.setBits( (unsigned char)prescalerValue, (unsigned char)PPRE2, (unsigned char)PPRE2 + 2 );
}

void ResetClockControl::setAPB1Prescaler( APBPrescaler prescalerValue )
{
   registers->CFGR.setBits( (unsigned char)prescalerValue, (unsigned char)PPRE1, (unsigned char)PPRE1 + 2 );
}

void ResetClockControl::setAHBPrescaler( AHBPrescaler prescalerValue )
{
   registers->CFGR.setBits( (unsigned char)prescalerValue, (unsigned char)HPRE, (unsigned char)HPRE + 3 );
}

ResetClockControl::SWSClock ResetClockControl::getSystemClock( void )
{
   return (SWSClock)registers->CFGR.getBits( (unsigned char)SWS, (unsigned char)SWS + 1 );
}

void ResetClockControl::setSystemClock( SWSClock clock )
{
   registers->CFGR.setBits( (unsigned char)clock, (unsigned char)SW, (unsigned char)SW + 1 );
}

void ResetClockControl::setBitInterruptRegister( CIRBits bit )
{
   registers->CIR.setBit( (unsigned char)bit );
}

void ResetClockControl::resetBitInterruptRegister( CIRBits bit )
{
   registers->CIR.resetBit( (unsigned char)bit );
}

void ResetClockControl::enableAPB1Peripheral( APB1Peripherals peripheral )
{
   registers->APB1ENR.setBit( (unsigned char)peripheral );
}

void ResetClockControl::enableAPB2Peripheral( APB2Peripherals peripheral )
{
   registers->APB2ENR.setBit( (unsigned char)peripheral );
}

void ResetClockControl::enableAHBPeripheral( AHBPeripherals peripheral )
{
   registers->AHBENR.setBit( (unsigned char)peripheral );
}

void ResetClockControl::resetAPB1Peripheral( APB1Peripherals peripheral )
{
   registers->APB1RSTR.setBit( (unsigned char)peripheral );
}

void ResetClockControl::resetAPB2Peripheral( APB2Peripherals peripheral )
{
   registers->APB2RSTR.setBit( (unsigned char)peripheral );
}

void ResetClockControl::resetAHBPeripheral( AHBPeripherals peripheral )
{
#ifdef STM32F10X_CL
   registers->AHBRSTR.setBit( (unsigned char)peripheral );
#endif
}

void ResetClockControl::resetEthernetMAC()
{

}

void ResetClockControl::resetUSB_OTGFS()
{

}

void ResetClockControl::configurePREDIV1( PREDIV1Factor prediv )
{
}

void ResetClockControl::configurePREDIV2( PREDIV2Factor prediv )
{

}

void ResetClockControl::configurePLL2MUL( PLL2MULFactor factor )
{

}

void ResetClockControl::configurePLL3MUL( PLL3MULFactor factor )
{

}

void ResetClockControl::configurePREDIV1SRC( bool hseClock )
{

}

void ResetClockControl::configureI2S2ClockSource( bool sysclk )
{

}

void ResetClockControl::configureI2S3ClockSource( bool sysclk )
{

}

void ResetClockControl::setSysClockTo24()
{
   volatile unsigned long startUpCounter = 0;
   //1. Enable HSE and Wait til the HSE is ready or the timer is elapsed
   enableHSE();
   while ( !isHSEReady() && startUpCounter < 0x500 )
   {
      startUpCounter++;
   }

   //The following configuration must be done only when PLL is disabled
   //2. Do not prescale any peripheral bus
   setAHBPrescaler( SYSCLK_NO );
   setAPB1Prescaler( HCLK_NO );
   setAPB2Prescaler( HCLK_NO );

   //-> clock = 8 / 2 = 4 MHz
   divideHSEForPLLEntry( true );

   //clock = clock * 6 = 24MHz;
   setPLLMultiplicationFactor( PLL_6 );

   //Select HSE through PREDIV1 as PLL entry clock
   selectPLLSource( false );

   //NOW enable PLL and wait till it is ready
   enablePLL();
   while ( !isPLLReady() ) { }

   //Select PLL as source of the system clock
   setSystemClock( PLL );

   //wait till PLL is used as System clock source
   while ( getSystemClock() != PLL ) { }


}

void ResetClockControl::setSysClockTo48()
{
   volatile unsigned long startUpCounter = 0;
      //1. Enable HSE and Wait til the HSE is ready or the timer is elapsed
      enableHSE();
      while ( !isHSEReady() && startUpCounter < 0x500 )
      {
         startUpCounter++;
      }

      //The following configuration must be done only when PLL is disabled
      //2. Do not prescale any peripheral bus
      setAHBPrescaler( SYSCLK_NO );
      setAPB1Prescaler( HCLK_NO );
      setAPB2Prescaler( HCLK_NO );

      //-> clock = 8 / 2 = 4 MHz
      divideHSEForPLLEntry( true );

      //clock = clock * 12 = 48MHz;
      setPLLMultiplicationFactor( PLL_12 );

      //Select HSE through PREDIV1 as PLL entry clock
      selectPLLSource( false );

      //NOW enable PLL and wait till it is ready
      enablePLL();
      while ( !isPLLReady() ) { }

      //Select PLL as source of the system clock
      setSystemClock( PLL );

      //wait till PLL is used as System clock source
      while ( getSystemClock() != PLL ) { }
}
