/*
 * InterruptController.hpp
 *
 *  Created on: 13.11.2020
 *      Author: Djoum
 */

#ifndef INTERRUPTCONTROLLER_HPP_
#define INTERRUPTCONTROLLER_HPP_

#include "Registers.hpp"
#include "SystemControlBlock.hpp"

namespace CortexM
{

class InterruptController
{
public:
   static InterruptController& instance()
   {
      static InterruptController nvic;
      return nvic;
   }


   struct NVICRegisters
   {
      ULong InterruptSetEnable[8];                 /*!< Offset: 0x000 (R/W)  Interrupt Set Enable Register           */
      ULong RESERVED0[24];
      ULong InterruptClearEnable[8];               /*!< Offset: 0x080 (R/W)  Interrupt Clear Enable Register         */
      ULong RSERVED1[24];
      ULong InterruptSetPending[8];                /*!< Offset: 0x100 (R/W)  Interrupt Set Pending Register          */
      ULong RESERVED2[24];
      ULong InterruptClearPending[8];              /*!< Offset: 0x180 (R/W)  Interrupt Clear Pending Register        */
      ULong RESERVED3[24];
      ULong InterruptActiveBit[8];                 /*!< Offset: 0x200 (R/W)  Interrupt Active bit Register           */
      ULong RESERVED4[56];
      UByte InterruptPriority[240];                /*!< Offset: 0x300 (R/W)  Interrupt Priority Register (8Bit wide) */
      ULong RESERVED5[644];
      ULong SoftwareTriggerInterruptRegister;      /*!< Offset: 0xE00 ( /W)  Software Trigger Interrupt Register     */
   };

   void enableInterrupt( const uint8_t irqn )
   {
      registers->InterruptSetEnable[((unsigned long)(irqn) >> 5)].aValue = (1 << ((unsigned long)(irqn) & 0x1F)); /* enable interrupt */
   }

   void disableInterrupt( const uint8_t irqn )
   {
      registers->InterruptClearEnable[((uint32_t)(irqn) >> 5)].aValue = (1 << ((uint32_t)(irqn) & 0x1F)); /* disable interrupt */
   }

   /** \brief  Get Pending Interrupt

       This function reads the pending register in the NVIC and returns the pending bit
       for the specified interrupt.

       \param [in]      IRQn  Number of the interrupt for get pending
       \return             0  Interrupt status is not pending
       \return             1  Interrupt status is pending
    */
   inline uint32_t getPendingIRQ(uint8_t IRQn)
   {
     return((uint32_t) ((registers->InterruptSetPending[(uint32_t)(IRQn) >> 5].aValue & (1 << ((uint32_t)(IRQn) & 0x1F)))?1:0)); /* Return 1 if pending else 0 */
   }


   /** \brief  Set Pending Interrupt

       This function sets the pending bit for the specified interrupt.
       The interrupt number cannot be a negative value.

       \param [in]      IRQn  Number of the interrupt for set pending
    */
   inline void setPendingIRQ(uint8_t IRQn)
   {
      registers->InterruptSetPending[((uint32_t)(IRQn) >> 5)].aValue = (1 << ((uint32_t)(IRQn) & 0x1F)); /* set interrupt pending */
   }


   /** \brief  Clear Pending Interrupt

       This function clears the pending bit for the specified interrupt.
       The interrupt number cannot be a negative value.

       \param [in]      IRQn  Number of the interrupt for clear pending
    */
   inline void clearPendingIRQ(uint8_t IRQn)
   {
      registers->InterruptClearPending[((uint32_t)(IRQn) >> 5)].aValue = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
   }


   /** \brief  Get Active Interrupt

       This function reads the active register in NVIC and returns the active bit.
       \param [in]      IRQn  Number of the interrupt for get active
       \return             0  Interrupt status is not active
       \return             1  Interrupt status is active
    */
   inline uint32_t getActive(uint8_t IRQn)
   {
     return((uint32_t)((registers->InterruptActiveBit[(uint32_t)(IRQn) >> 5].aValue & (1 << ((uint32_t)(IRQn) & 0x1F)))?1:0)); /* Return 1 if active else 0 */
   }

   inline void triggerInterrupt( const uint8_t irqn )
   {

   }

   void setInterruptHandler( const uint8_t irqn, void (*handler)( void ) )
   {
      unsigned char newIrq = 0;
      if ( irqn < 0 )
      {
         newIrq = irqn & 0xF;
      }
      else
      {
         newIrq = irqn + 16;
      }
      //unsigned long * table = reinterpret_cast<unsigned long *>( SRAM_BASE );

      unsigned long * table = SystemControlBlock::instance().getVectorTable();

      table[newIrq] = reinterpret_cast< unsigned long >(handler);
   }

private:
   InterruptController() : address( NVIC_BASE ), registers( reinterpret_cast<NVICRegisters volatile*>(NVIC_BASE) )
   {}

   unsigned long address;
   NVICRegisters volatile* registers;
};

} /* namespace CortexM */

#endif /* INTERRUPTCONTROLLER_HPP_ */
