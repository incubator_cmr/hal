/*
 * Callback.hpp
 *
 *  Created on: 10.02.2021
 *      Author: Djoum
 */

#ifndef CALLBACK_HPP_
#define CALLBACK_HPP_


class CallbackInterface
{
public:
   virtual void operator()() {};
};

template<typename TheClass>
class Callback : public CallbackInterface
{
public:
   Callback( TheClass& _object, void(TheClass::*_member)(void) ):
      object(_object),
      member(_member)
   {
   }

   virtual void operator()( ) override
   {
      (object.*member)();
   }

private:
   void (TheClass::*member)(void);
   TheClass& object;
};


#endif /* CALLBACK_HPP_ */
