/*
 * GPIOPort.hpp
 *
 *  Created on: 15.11.2020
 *      Author: Djoum
 */

#ifndef GPIOPORT_HPP_
#define GPIOPORT_HPP_

#include "Registers.hpp"
#include "stm32f10x.h"

class GPIOPort
{
public:
   GPIOPort( unsigned long _address ) :
      address( _address ),
      registers( reinterpret_cast<GPIORegisters volatile*>(address) )

   {

   }

   struct GPIORegisters
   {
      ULong CRL;  //0x00
      ULong CRH;  //0x04
      ULong IDR;  //0x08
      ULong ODR;  //0x0C
      ULong BSRR; //0x10
      ULong BRR;  //0x14
      ULong LCKR; //0x18
   };

   //--- CNFy[1:0]
   typedef enum
   {
      ANALOG = 0, FLOATING, PULLUP_PULLDOWN, RESERVED
   } InputMode;

   typedef enum
   {
      GP_PUSH_PULL = 0, GP_OPEN_DRAIN, AF_PUSH_PULL, AF_OPEN_DRAIN
   } OutputMode;
   //-----

   //--- MODEy[1:0]
   typedef enum
   {
      INPUT = 0, SPEED_10MHz, SPEED_2MHz, SPEED_50MHz
   } Speed;
   //----

   inline void configureInput( unsigned char pin, InputMode mode )
   {
      unsigned char offset = pin * 0x04;
      unsigned char confOffset = offset + 2;

      if ( offset < 32 )
      {
         //CRL
         registers->CRL.setBits( (unsigned char)mode, confOffset,
            confOffset + 1 );
      }
      else
      {
         //CRH
         registers->CRH.setBits( (unsigned char)mode, confOffset,
            confOffset + 1 );
      }
   }

   void configureOutput( unsigned char pin, OutputMode mode, Speed speed )
   {
      unsigned char modeOffset = pin * 0x04;
      unsigned char confOffset = modeOffset + 2;

      if ( modeOffset < 32 )
      {
         //CRL
         registers->CRL.setBits( (unsigned char)mode, confOffset,
            confOffset + 1 );
         registers->CRL.setBits( (unsigned char)speed, modeOffset, modeOffset + 1 );
      }
      else
      {
         modeOffset -= 32;
         confOffset = modeOffset + 2;
         //CRH
         registers->CRH.setBits( (unsigned char)mode, confOffset,
            confOffset + 1 );
         registers->CRH.setBits( (unsigned char)speed, modeOffset, modeOffset + 1 );
      }
   }

   bool readInputDataBit( unsigned char pin )
   {
      return registers->IDR.getBits( pin, pin ) ? true : false;
   }

   bool readOutputDataBit( unsigned char pin )
   {
      return registers->ODR.getBits( pin, pin ) ? true : false;
   }

   void setPin( unsigned char pin )
   {
      registers->BSRR.setBit( pin );
   }

   void resetPin( unsigned char pin )
   {
      registers->BSRR.setBit( pin + 0x10 );
   }

private:
   unsigned long address;
   GPIORegisters volatile* registers;

};

class PortA : public GPIOPort
{
public:

   static PortA& instance()
   {
      static PortA port;
      return port;
   }

private:
   PortA() :
      GPIOPort( GPIOA_BASE )
   {

   }
};

class PortB : public GPIOPort
{
public:

   static PortB& instance()
   {
      static PortB port;
      return port;
   }

private:
   PortB() :
      GPIOPort( GPIOB_BASE )
   {

   }
};

class PortC : public GPIOPort
{
public:

   static PortC& instance()
   {
      static PortC port;
      return port;
   }

private:
   PortC() :
      GPIOPort( GPIOC_BASE )
   {

   }
};

class PortD : public GPIOPort
{
public:

   static PortD& instance()
   {
      static PortD port;
      return port;
   }

private:
   PortD() :
      GPIOPort( GPIOD_BASE )
   {

   }
};

class PortE : public GPIOPort
{
public:

   static PortE& instance()
   {
      static PortE port;
      return port;
   }

private:
   PortE() :
      GPIOPort( GPIOE_BASE )
   {

   }
};

class PortF : public GPIOPort
{
public:

   static PortF& instance()
   {
      static PortF port;
      return port;
   }

private:
   PortF() :
      GPIOPort( GPIOF_BASE )
   {

   }
};

#endif /* GPIOPORT_HPP_ */
