/*
 * SystemControlBlock.hpp
 *
 *  Created on: 13.11.2020
 *      Author: Djoum
 */

#ifndef SYSTEMCONTROLBLOCK_HPP_
#define SYSTEMCONTROLBLOCK_HPP_

#include "Registers.hpp"
#include "stm32f10x.h"
#include <core_cm3.h>


namespace CortexM
{

class SystemControlBlock
{
public:

   static SystemControlBlock& instance()
   {
      static SystemControlBlock nvic;
      return nvic;
   }

      struct SCBRegisters
      {
         ULong CPUID;
         ULong ICSR;
         ULong VTOR;
         ULong AIRCR; //AIRCR
         ULong SCR;
         ULong CCR;
         UByte SHP[12];
         ULong SHCSR;
         ULong CFSR;
         ULong HFSR;
         ULong DFSR;
         ULong MMFAR;
         ULong BFAR;
         ULong AFSR;
         ULong PFR[2];
         UShort DFR;
         ULong AFR;
         ULong MMFR[4];
         ULong ISAR[5];
         ULong CPACR;
      };

      enum ICSRBits
      {
         eNMIPENDSET     = 31,
         ePENDSVSET      = 28,
         ePENDSVCLR      = 27,
         ePENDSTSET      = 26,
         ePENDSTCLR      = 25,
         eISRPRE_EMPT    = 23,
         eISRPENDING     = 22,
         eVECTPEND_START = 12,
         eVECTPEND_END   = 21,
         eRETTOBASE      = 11,
         eVECTACTIVE     = 0 ,
         eVECTACTIVE_END = 9
      };




      enum AIRCRBits
      {
         eVectKey_End      = 31,
         eVectKey_Start    = 16,
         eEndianness       = 15,
         ePriGroup_End     = 10,
         ePriGroup_Start   = 8,
         eSystemResetReq   = 2,
         eVectClrActive    = 1,
         eVectReset        = 0
      };

      uint8_t getCurrentRunningISR( void )
      {
         return registers->ICSR.getBits(eVECTPEND_START, eVECTPEND_END);
      }

      void returnToThreadLevel( void )
      {
         registers->ICSR.setBit(eRETTOBASE);
      }

      uint8_t getPendingISR( void )
      {
         return registers->ICSR.getBits(eVECTPEND_START, eVECTPEND_END);
      }

      bool isExternalISRPending( void )
      {
         return ( registers->ICSR.getBits(eISRPENDING, eISRPENDING) & 0x01 );
      }

      void setSysTickPendingStatus( const bool set = true )
      {
         if ( set )
         {
            registers->ICSR.setBit(ePENDSTSET);
         }
         else
         {
            registers->ICSR.setBit(ePENDSTCLR);
         }
      }

      void setPendSVPendingStatus( const bool set = true )
      {
         if ( set )
         {
            registers->ICSR.setBit(ePENDSVSET);
         }
         else
         {
            registers->ICSR.setBit(ePENDSVCLR);
         }
      }

      void setNMIPendingStatus( void )
      {
         registers->ICSR.setBit(eNMIPENDSET);
      }

      void systemReset( void )
      {
         __DSB();
         registers->AIRCR.setBits(0x5FA, eVectKey_Start, eVectKey_End);
         registers->AIRCR.setBit(eSystemResetReq);
         __DSB();

         //wait until the reset is triggered
         while( 1 );
      }

      bool isBigEndian( void )
      {
         return ( registers->AIRCR.getBits(eEndianness, eEndianness) & 0x01 );
      }

      void relocateVectorTable( const unsigned long newAddress )
      {
         __DMB();
         registers->VTOR.aValue = newAddress;
         __DSB();

         vectorTable = reinterpret_cast<unsigned long *>( registers->VTOR.aValue );
      }

      unsigned long * getVectorTable( void )
      {
         return vectorTable;
      }

private:
   SystemControlBlock() : address(SCB_BASE), registers( reinterpret_cast<SCBRegisters volatile*>( SCB_BASE ) )
   {
      vectorTable = reinterpret_cast<unsigned long *>( registers->VTOR.getBits(7, 31) );
   }

   unsigned long address;
   unsigned long * vectorTable;
   SCBRegisters volatile* registers;
};

} /* namespace CortexM */

#endif /* SYSTEMCONTROLBLOCK_HPP_ */
