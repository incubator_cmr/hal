/*
 * InputPin.hpp
 *
 *  Created on: 15.11.2020
 *      Author: Djoum
 */

#ifndef INPUTPIN_HPP_
#define INPUTPIN_HPP_

#include "GPIOPort.hpp"
#include "GPIOPin.hpp"

class InputPin : public GPIOPin
{
public:
   InputPin( GPIOPort& port, unsigned char pin, GPIOPort::InputMode mode ) :
      GPIOPin( &port, pin ), aMode( mode )
   {
      configure( aMode );
   }

   InputPin( GPIOPort& port, unsigned char pin ) :
      GPIOPin( &port, pin ), aMode( GPIOPort::FLOATING )
   {
      configure( aMode );
   }

   bool read()
   {
      return port->readInputDataBit( aPin );
   }

   void configure( GPIOPort::InputMode mode )
   {
      port->configureInput( aPin, mode );
   }

private:
   GPIOPort::InputMode aMode;
};

#endif /* INPUTPIN_HPP_ */
