/*
 * SystemTimer.cpp
 *
 *  Created on: 12.11.2020
 *      Author: Djoum
 */

#include "SystemTimer.hpp"
#include "SystemTime.hpp"

SystemTimer::SystemTimer(const unsigned long _timer ) : startTime(0), timerValue( _timer ), isRunning( false ), isPause( false )
{

}

void SystemTimer::start( void )
{
   if ( isPause )
   {
      //Just continue
      isPause = false;
   }
   else if ( !isRunning )
   {
      isRunning = true;
      startTime = SystemTime::instance().now();
   }

}

void SystemTimer::pause( void )
{
   isPause = true;
}

void SystemTimer::stop( void )
{
   isRunning = false;
   isPause = false;
}

unsigned long SystemTimer::since( void )
{
   if ( isRunning )
   {
      return SystemTime::instance().now() - startTime;
   }
   return 0;

}

bool SystemTimer::isElapsed( void )
{
   if (isRunning)
   {
      if (since() < timerValue)
      {
         return false;
      }
      else
      {
         isRunning = false;
         isPause = false;
         return true;
      }
   }
   else
   {
      return false;
   }
}

void SystemTimer::wait( const unsigned long waitingTime )
{
   SystemTimer myTimer(waitingTime);
   myTimer.start();

   while ( !myTimer.isElapsed() )
   {
      //Do nothing
   }
}

unsigned long SystemTimer::now( void )
{
   return SystemTime::instance().now();
}





