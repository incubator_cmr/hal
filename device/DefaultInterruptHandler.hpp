/*
 * DefaultInterruptHandler.hpp
 *
 *  Created on: 13.11.2020
 *      Author: Djoum
 */

#ifndef DEFAULTINTERRUPTHANDLER_HPP_
#define DEFAULTINTERRUPTHANDLER_HPP_

class DefaultInterruptHandler
{
public:
   static void NMI_Handler(void);

   static void HardFault_Handler(void);

   static void MemManage_Handler(void);

   static void BusFault_Handler(void);

   static void UsageFault_Handler(void);

   static void SVC_Handler(void);

   static void DebugMon_Handler(void);

   static void PendSV_Handler(void);

   static void SysTick_Handler(void);

   static void usb_Handler(void);
};





#endif /* DEFAULTINTERRUPTHANDLER_HPP_ */
