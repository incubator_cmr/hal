/*
 * OutputPin.hpp
 *
 *  Created on: 15.11.2020
 *      Author: Djoum
 */

#ifndef OUTPUTPIN_HPP_
#define OUTPUTPIN_HPP_

#include "GPIOPort.hpp"
#include "GPIOPin.hpp"

class OutputPin : public GPIOPin
{
public:
   OutputPin( GPIOPort& port, unsigned char pin, GPIOPort::OutputMode mode,
      GPIOPort::Speed speed ) :
      GPIOPin( &port, pin ), aMode( mode ), aSpeed( speed )
   {
      configure( aMode, aSpeed );
   }

   OutputPin( GPIOPort& port, unsigned char pin ) :
      GPIOPin( &port, pin ), aMode( GPIOPort::GP_OPEN_DRAIN ), aSpeed(
         GPIOPort::SPEED_2MHz )
   {
      configure( aMode, aSpeed );
   }

   void set()
   {
      port->setPin( aPin );
   }

   void reset()
   {
      port->resetPin( aPin );
   }

   void toggle()
   {
      if ( port->readOutputDataBit( aPin ) )
      {
         reset();
      }
      else
      {
         set();
      }
   }

   void configure( GPIOPort::OutputMode mode, GPIOPort::Speed speed )
   {
      aMode = mode;
      aSpeed = speed;

      port->configureOutput( aPin, mode, speed );
   }

private:
   GPIOPort::OutputMode aMode;
   GPIOPort::Speed aSpeed;
};

#endif /* OUTPUTPIN_HPP_ */
