/*
 * SysTick.hpp
 *
 *  Created on: 12.11.2020
 *      Author: Djoum
 */

#ifndef SYSTICK_HPP_
#define SYSTICK_HPP_

#include "Registers.hpp"
#include "stm32f10x.h"
#

class SystemTick
{
public:
   static SystemTick& instance( void )
   {
      static SystemTick systick;
      return systick;
   }

   struct SysTickRegisters
   {
      ULong CTRL;    //!< SYSTICK Control and Status Register (0xE000E010)
      ULong LOAD;    //!<
      ULong VAL;     //!<
      ULong CALIB;   //!<
   };

   enum ControlStatusRegisterBits
   {
      ENABLE      = 0,
      TICKINT     = 1,
      CLKSOURCE   = 2,
      COUNTFLAG   = 16
   };

   enum ReloadValueRegisterBits
   {
      RELOAD_START   = 0,
      RELOAD_END     = 23
   };

   enum CurrentValueRegisterBits
   {
      CURRENT_START  = 0, // from 0 to 23
      CURRENT_END    = 23
   };

   enum CalibrationRegisterBits
   {
      TENMS_START = 0,
      TENMS_END   = 23,
      SKEW        = 30,
      NOREF       = 31
   };

   void enable( const bool enable = true )
   {
      if ( enable )
      {
         registers->CTRL.setBit(ENABLE);
         registers->CTRL.setBit(TICKINT);
      }
      else
      {
         registers->CTRL.resetBit(ENABLE);
         registers->CTRL.resetBit(TICKINT);
      }

      //Wait until count flag ist set
      while ( registers->CTRL.getBits(COUNTFLAG, COUNTFLAG) == 0)
      {
         //Do nothing
      }
   }

   inline bool isEnable( void )
   {
      return registers->CTRL.getBits(ENABLE, ENABLE);
   }

   void configure( const unsigned long inputClkFreq, const unsigned long tick, const bool useCoreClk = true )
   {
      //First calculate the reload value
      unsigned long reloadValue = inputClkFreq / tick;
      //Reduce the reload value by 12 cycles to compensate for the interrupt latency
      registers->LOAD.setBits(reloadValue - 1, RELOAD_START, RELOAD_END);

      //Set the counter value to 0. This will ensure that the systick start to count over right after being enabled
      registers->VAL.setBits(0, CURRENT_START, CURRENT_END);

      if ( useCoreClk )
      {
         registers->CTRL.setBit(CLKSOURCE);
      }
      else
      {
         registers->CTRL.resetBit(CLKSOURCE);
      }

      NVIC_SetPriority (SysTick_IRQn, (1<<__NVIC_PRIO_BITS) - 1);


   }

   static void interruptHandler( void )
   {
      if ( currentTick >= 0xFFFFFFFF )
      {
         currentTick = 0;
      }
      currentTick++;
   }

   static unsigned long getCurrentTick()
   {
      return currentTick;
   }

private:
   SystemTick();
   SysTickRegisters volatile* registers;

   static unsigned long currentTick;
};

#endif /* SYSTICK_HPP_ */
