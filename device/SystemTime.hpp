/*
 * SystemTime.hpp
 *
 *  Created on: 12.11.2020
 *      Author: Djoum
 */

#ifndef SYSTEMTIME_HPP_
#define SYSTEMTIME_HPP_

#include "SysTick.hpp"

class SystemTime
{
public:
   static SystemTime& instance( void )
   {
      static SystemTime systemTime;
      return systemTime;
   }

   unsigned long now( void )
   {
      return SystemTick::getCurrentTick();
   }

private:
   SystemTime();
};

#endif /* SYSTEMTIME_HPP_ */
