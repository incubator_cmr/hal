/*
 * ResetClockControl.hpp
 *
 *  Created on: 15.11.2020
 *      Author: Djoum
 */

#ifndef RESETCLOCKCONTROL_HPP_
#define RESETCLOCKCONTROL_HPP_

#include "Registers.hpp"
#include "stm32f10x.h"

class ResetClockControl
{
public:

   static ResetClockControl& instance()
   {
      static ResetClockControl rcc;
      return rcc;
   }

private:

   ResetClockControl(): address( RCC_BASE ), registers(reinterpret_cast<RCCRegisters volatile*>(RCC_BASE))
   {
   }

   struct RCCRegisters
   {
      ULong CR;
      ULong CFGR;
      ULong CIR;
      ULong APB2RSTR;
      ULong APB1RSTR;
      ULong AHBENR;
      ULong APB2ENR;
      ULong APB1ENR;
      ULong BDCR;
      ULong CSR;

#ifdef STM32F10X_CL
      ULong AHBRSTR;
      ULong CFGR2;
#endif /* STM32F10X_CL */

#if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
      uint32_t RESERVED0;
      ULong CFGR2;
#endif /* STM32F10X_LD_VL || STM32F10X_MD_VL || STM32F10X_HD_VL */
   } ;

private:

   unsigned long address;
   RCCRegisters volatile*  registers;

public:
   typedef enum
   {
      HSION = 0,
      HSIRDY = 1,
      HSITRIM = 3,
      HSICAL = 8,
      HSEON = 16,
      HSERDY = 17,
      HSEBYP = 18,
      CSSON = 19,
      PLLON    = 24,
      PLLRDY   = 25,
      PLL2ON   = 26,
      PLL2RDY  = 27,
      PLL3ON   = 28,
      PLL3RDY  = 29,

   }CRBits;

   typedef enum
   {
      SW = 0,
      SWS = 2,
      HPRE = 4,
      PPRE1 = 8,
      PPRE2 = 11,
      ADCPRE = 14,
      PLLSRC = 16,
      PLLXTPRE = 17,
      PLLMUL = 18,
      USBPRE = 22,
      MCO = 24
   }CFGRBits;

   typedef struct
   {                                   // bit-pos:
      unsigned long sw       : 2;      // 1:0
      unsigned long sws      : 2;      // 3:2
      unsigned long hpre     : 4;      // 7:4
      unsigned long ppre1    : 3;      // 10:8
      unsigned long ppre2    : 3;      // 13:11
      unsigned long adcpre   : 2;      // 15:14
      unsigned long pllsrc   : 1;      // 16
      unsigned long pllxtpre : 1;      // 17
      unsigned long pllmul   : 4;      // 21:18
      unsigned long usbpre   : 1;      // 22
      unsigned long          : 1;      // 23
      unsigned long mco      : 3;      // 26:24
      unsigned long          : 5;      // 31:27
   } ConfigRegisterBits;

   //! PLL clock ready flag
   /*!
      Set by hardware to indicate that the PLL is locked.
      0: PLL unlocked
      1: PLL locked
   */
   bool isPLLReady();

   //! PLL enable
   /*!
      Set and cleared by software to enable PLL.
      Cleared by hardware when entering Stop or Standby mode. This bit can not be reset if the
      PLL clock is used as system clock or is selected to become the system clock.
      0: PLL OFF
      1: PLL ON
   */
   void enablePLL( bool enable = true );
   bool isPLLEnabled( void );

   //! Bit 19 CSSON: Clock security system enable
   /*!
      Set and cleared by software to enable the clock security system. When CSSON is set, the
      clock detector is enabled by hardware when the HSE oscillator is ready, and disabled by
      hardware if a HSE clock failure is detected.
      0: Clock detector OFF
      1: Clock detector ON (Clock detector ON if the HSE oscillator is ready , OFF if not).
   */
   void enableCSS( bool enable = true );
   bool isCSSEnabled( void );

   //! Bit 18 HSEBYP: External high-speed clock bypass
   /*!
      Set and cleared by software to bypass the oscillator with an external clock. The external
      clock must be enabled with the HSEON bit set, to be used by the device. The HSEBYP bit
      can be written only if the HSE oscillator is disabled.
      0: external 4-16 MHz oscillator not bypassed
      1: external 4-16 MHz oscillator bypassed with external clock
   */
   void bypassExternalClock( bool clockDetector = false );
   bool isExternalClockBypassed( void );

   //! Bit 17 HSERDY: External high-speed clock ready flag
   /*!
      Set by hardware to indicate that the HSE oscillator is stable. This bit needs 6 cycles of the
      HSE oscillator clock to fall down after HSEON reset.
      0: HSE oscillator not ready
      1: HSE oscillator ready
   */
   bool isHSEReady( void );

   //! Bit 16 HSEON: HSE clock enable
   /*!
      Set and cleared by software.
      Cleared by hardware to stop the HSE oscillator when entering Stop or Standby mode. This
      bit cannot be reset if the HSE oscillator is used directly or indirectly as the system clock.
      0: HSE oscillator OFF
      1: HSE oscillator ON
   */
   void enableHSE( bool enable = true );
   bool isHSEEnabled( void );

   //! Bits 15:8 HSICAL[7:0]: Internal high-speed clock calibration
   /*!
      These bits are initialized automatically at startup.
   */
   unsigned char getHSICalibrationValue( void );

   //! Bits 7:3 HSITRIM[4:0]: Internal high-speed clock trimming
   /*!
      These bits provide an additional user-programmable trimming value that is added to the
      HSICAL[7:0] bits. It can be programmed to adjust to variations in voltage and temperature
      that influence the frequency of the internal HSI RC.
      The default value is 16, which, when added to the HSICAL value, should trim the HSI to 8
      MHz � 1%. The trimming step (Fhsitrim) is around 40 kHz between two consecutive HSICAL
      steps.
   */
   void trimHSI( unsigned char trimmingValue );
   unsigned char getTrimmingValue( void );

   //! Bit 1 HSIRDY: Internal high-speed clock ready flag
   /*!
      Set by hardware to indicate that internal 8 MHz RC oscillator is stable. After the HSION bit is
      cleared, HSIRDY goes low after 6 internal 8 MHz RC oscillator clock cycles.
      0: internal 8 MHz RC oscillator not ready
      1: internal 8 MHz RC oscillator ready
   */
   bool isHSIReady( void );


   //! Bit 0 HSION: Internal high-speed clock enable
   /*!
      Set and cleared by software.
      Set by hardware to force the internal 8 MHz RC oscillator ON when leaving Stop or Standby
      mode or in case of failure of the external 4-16 MHz oscillator used directly or indirectly as
      system clock. This bit cannot be reset if the internal 8 MHz RC is used directly or indirectly
      as system clock or is selected to become the system clock.
      0: internal 8 MHz RC oscillator OFF
      1: internal 8 MHz RC oscillator ON
   */
   void enableHSI( bool enable = true );
   bool isHSIEnabled( void );

   typedef enum
   {
      SYSCLK  = 0b100,
      HSI_CLK = 0b101,
      HSE_CLK = 0b110,
      PLL_CLK = 0b111
   }ClockOutput;
   //! Bits 26:24 MCO: Microcontroller clock output
   /*!
      Set and cleared by software.
      0xx: No clock
      100: System clock (SYSCLK) selected
      101: HSI clock selected
      110: HSE clock selected
      111: PLL clock divided by 2 selected
      Note: This clock output may have some truncated cycles at startup or during MCO clock
      source switching.
      When the System Clock is selected to output to the MCO pin, make sure that this clock
      does not exceed 50 MHz (the maximum IO speed).
   */
   void setClockOutput( ClockOutput mco );

   //! Bit 22 USBPRE: USB prescaler
   /*!
      Set and cleared by software to generate 48 MHz USB clock. This bit must be valid before
      enabling the USB clock in the RCC_APB1ENR register. This bit can�t be reset if the USB
      clock is enabled.
      0: PLL clock is divided by 1.5
      1: PLL clock is not divided
   */
   void enableUSBPrescaler( bool clockIsDivided = true );

   typedef enum
   {
      PLL_2 = 0b0000,
      PLL_3 = 0b0001,
      PLL_4 = 0b0010,
      PLL_5 = 0b0011,
      PLL_6 = 0b0100,
      PLL_7 = 0b0101,
      PLL_8 = 0b0110,
      PLL_9 = 0b0111,
      PLL_10 = 0b1000,
      PLL_11 = 0b1001,
      PLL_12 = 0b1010,
      PLL_13 = 0b1011,
      PLL_14 = 0b1100,
      PLL_15 = 0b1101,
      PLL_16 = 0b1110,
      PLL_16_1 = 0b1111
   }PLLInputClock;

   //! Bits 21:18 PLLMUL: PLL multiplication factor
   /*!
      These bits are written by software to define the PLL multiplication factor. These bits can be
      written only when PLL is disabled.
      Caution: The PLL output frequency must not exceed 72 MHz.
      0000: PLL input clock x 2
      0001: PLL input clock x 3
      0010: PLL input clock x 4
      0011: PLL input clock x 5
      0100: PLL input clock x 6
      0101: PLL input clock x 7
      0110: PLL input clock x 8
      0111: PLL input clock x 9
      1000: PLL input clock x 10
      1001: PLL input clock x 11
      1010: PLL input clock x 12
      1011: PLL input clock x 13
      1100: PLL input clock x 14
      1101: PLL input clock x 15
      1110: PLL input clock x 16
      1111: PLL input clock x 16
   */
   void setPLLMultiplicationFactor( PLLInputClock factor );

   //! Bit 17 PLLXTPRE: HSE divider for PLL entry
   /*!
      Set and cleared by software to divide HSE before PLL entry. This bit can be written only
      when PLL is disabled.
      0: HSE clock not divided
      1: HSE clock divided by 2
   */
   void divideHSEForPLLEntry( bool divide = false );

   //! Bit 16 PLLSRC: PLL entry clock source
   /*!
      Set and cleared by software to select PLL clock source. This bit can be written only when
      PLL is disabled.
      0: HSI oscillator clock / 2 selected as PLL input clock
      1: HSE oscillator clock selected as PLL input clock
   */
   void selectPLLSource( bool hsiDividedBy2 = true );

   typedef enum
   {
      PCLK2_2 = 0b00,
      PCLK2_4 = 0b01,
      PCLK2_6 = 0b10,
      PCLK2_8 = 0b11
   }ADCPrescaler;
   //! Bits 15:14 ADCPRE: ADC prescaler
   /*!
      Set and cleared by software to select the frequency of the clock to the ADCs.
      00: PCLK2 divided by 2
      01: PCLK2 divided by 4
      10: PCLK2 divided by 6
      11: PCLK2 divided by 8
   */
   void setADCPrescaler( ADCPrescaler prescalerValue );

   typedef enum
   {
      HCLK_NO = 0b000,
      HCLK_2 = 0b100,
      HCLK_4 = 0b101,
      HCLK_6 = 0b110,
      HCLK_8 = 0b111
   }APBPrescaler;

   //! Bits 13:11 PPRE2: APB high-speed prescaler (APB2)
   /*!
      Set and cleared by software to control the division factor of the APB high-speed clock
      (PCLK2).
      0xx: HCLK not divided
      100: HCLK divided by 2
      101: HCLK divided by 4
      110: HCLK divided by 8
      111: HCLK divided by 16
   */
   void setAPB2Prescaler( APBPrescaler prescalerValue );



   //! Bits 10:8 PPRE1: APB low-speed prescaler (APB1)
   /*!
      Set and cleared by software to control the division factor of the APB low-speed clock
      (PCLK1).
      Warning: the software has to set correctly these bits to not exceed 36 MHz on this domain.
      0xx: HCLK not divided
      100: HCLK divided by 2
      101: HCLK divided by 4
      110: HCLK divided by 8
      111: HCLK divided by 16
   */
   void setAPB1Prescaler( APBPrescaler prescalerValue );

   typedef enum
   {
      SYSCLK_NO = 0b0000,
      SYSCLK_2 = 0b1000,
      SYSCLK_4 = 0b1001,
      SYSCLK_8 = 0b1010,
      SYSCLK_16 = 0b1011,
      SYSCLK_64 = 0b1100,
      SYSCLK_128 = 0b1101,
      SYSCLK_256 = 0b1110,
      SYSCLK_512 = 0b1111
   }AHBPrescaler;
   //! Bits 7:4 HPRE: AHB prescaler
   /*!
      Set and cleared by software to control the division factor of the AHB clock.
      0xxx: SYSCLK not divided
      1000: SYSCLK divided by 2
      1001: SYSCLK divided by 4
      1010: SYSCLK divided by 8
      1011: SYSCLK divided by 16
      1100: SYSCLK divided by 64
      1101: SYSCLK divided by 128
      1110: SYSCLK divided by 256
      1111: SYSCLK divided by 512
      Note: The prefetch buffer must be kept on when using a prescaler different from 1 on the
      AHB clock. Refer to Reading the Flash memory section for more details.
   */
   void setAHBPrescaler( AHBPrescaler prescalerValue );

   typedef enum
   {
      HSI = 0,
      HSE,
      PLL,
      N_A
   }SWSClock;

   //! Bits 3:2 SWS: System clock switch status
   /*!
      Set and cleared by hardware to indicate which clock source is used as system clock.
      00: HSI oscillator used as system clock
      01: HSE oscillator used as system clock
      10: PLL used as system clock
      11: not applicable
   */
   SWSClock getSystemClock( void );

   //! Bits 1:0 SW: System clock switch
   /*!
      Set and cleared by software to select SYSCLK source.
      Set by hardware to force HSI selection when leaving Stop and Standby mode or in case of
      failure of the HSE oscillator used directly or indirectly as system clock (if the Clock Security
      System is enabled).
      00: HSI selected as system clock
      01: HSE selected as system clock
      10: PLL selected as system clock
      11: not allowed
   */
   void setSystemClock( SWSClock clock );

   //CIR_Bitfield
   //CLR = CLEAR
   //IE = Interrupt Enable
   //F = Flag
   typedef enum
   {
      CSSCLR = 23,
      PLLRDYCLR = 20,
      HSERDYCLR = 19,
      HSIRDYCLR = 18,
      LSERDYCLR = 17,
      LSIRDYCLR = 16,

      PLLRDYIE = 12,
      HSERDYIE = 11,
      HSIRDYIE = 10,
      LSERDYIE = 9,
      LSIRDYIE = 8,

      CSSF = 7,
      PLLRDYF = 4,
      HSERDYF = 3,
      HSIRDYF = 2,
      LSERDYF = 1,
      LSIRDYF = 0
   }CIRBits;

   void setBitInterruptRegister( CIRBits  bit );
   void resetBitInterruptRegister( CIRBits  bit );

   typedef enum
   {
      APB2_AFIO = 0,
      APB2_IOPA = 2,
      APB2_IOPB,
      APB2_IOPC,
      APB2_IOPD,
      APB2_IOPE,
      APB2_IOPF,
      APB2_IOPG,
      APB2_ADC1,
      APB2_ADC2,
      APB2_TIM1,
      APB2_SPI1,
      APB2_TIM8,
      APB2_USART1,
      APB2_ADC3,

      APB2_TIM9 = 19,
      APB2_TIM10,
      APB2_TIM11

   }APB2Peripherals;

   typedef enum
   {
      APB1_TIM2 = 0,
      APB1_TIM3,
      APB1_TIM4,
      APB1_TIM5,
      APB1_TIM6,
      APB1_TIM7,
      APB1_TIM12,
      APB1_TIM13,
      APB1_TIM14,
      APB1_WWDG = 11,
      APB1_SPI2 = 14,
      APB1_SPI3 = 15,
      APB1_USART2 = 17,
      APB1_USART3,
      APB1_USART4,
      APB1_USART5,
      APB1_I2C1,
      APB1_I2C2,
      APB1_USB,
      APB1_CAN = 25,
      APB1_BKP = 27,
      APB1_PWR,
      APB1_DAC
   }APB1Peripherals;

   typedef enum
   {
      AHB_DMA1 = 0,
      AHB_DMA2,
      AHB_SRAM_EN,

      AHB_FLITF = 4,

      AHB_CRC = 6,
      AHB_FSMC = 8,
      AHB_SDIO = 10
   }AHBPeripherals;


   void enableAPB1Peripheral( APB1Peripherals peripheral );
   void enableAPB2Peripheral( APB2Peripherals peripheral );
   void enableAHBPeripheral( AHBPeripherals peripheral );

   void resetAPB1Peripheral( APB1Peripherals peripheral );
   void resetAPB2Peripheral( APB2Peripherals peripheral );
   void resetAHBPeripheral( AHBPeripherals peripheral );

   void resetEthernetMAC();
   void resetUSB_OTGFS();

   typedef enum
   {
      PREDIV1 = 0,
      PREDIV2 = 4,
      PLL2MUL = 8,
      PLL3MUL = 12,
      PREDIV1SRC = 16,
      I2S2SRC = 17,
      I2S3SRC = 18
   }Configuration2Bits;

   typedef enum
   {
      PREDIV1_ND  = 0b0000,
      PREDIV1_2   = 0b0001,
      PREDIV1_3   = 0b0010,
      PREDIV1_4   = 0b0011,
      PREDIV1_5   = 0b0100,
      PREDIV1_6   = 0b0101,
      PREDIV1_7   = 0b0110,
      PREDIV1_8   = 0b0111,
      PREDIV1_9   = 0b1000,
      PREDIV1_10  = 0b1001,
      PREDIV1_11  = 0b1010,
      PREDIV1_12  = 0b1011,
      PREDIV1_13  = 0b1100,
      PREDIV1_14  = 0b1101,
      PREDIV1_15  = 0b1110,
      PREDIV1_16  = 0b1111
   }PREDIV1Factor;

   //! Bits 3:0 PREDIV1[3:0]: PREDIV1 division factor
   /*!
      Set and cleared by software to select PREDIV1 division factor. These bits can be written only
      when PLL is disabled.
      Note: Bit(0) is the same as bit(17) in the RCC_CFGR register, so modifying bit(17) in the
      RCC_CFGR register changes Bit(0) accordingly.
      0000: PREDIV1 input clock not divided
      0001: PREDIV1 input clock divided by 2
      0010: PREDIV1 input clock divided by 3
      0011: PREDIV1 input clock divided by 4
      0100: PREDIV1 input clock divided by 5
      0101: PREDIV1 input clock divided by 6
      0110: PREDIV1 input clock divided by 7
      0111: PREDIV1 input clock divided by 8
      1000: PREDIV1 input clock divided by 9
      1001: PREDIV1 input clock divided by 10
      1010: PREDIV1 input clock divided by 11
      1011: PREDIV1 input clock divided by 12
      1100: PREDIV1 input clock divided by 13
      1101: PREDIV1 input clock divided by 14
      1110: PREDIV1 input clock divided by 15
      1111: PREDIV1 input clock divided by 16
   */
   void configurePREDIV1( PREDIV1Factor prediv );

   typedef enum
   {
      PREDIV2_ND  = 0b0000,
      PREDIV2_2   = 0b0001,
      PREDIV2_3   = 0b0010,
      PREDIV2_4   = 0b0011,
      PREDIV2_5   = 0b0100,
      PREDIV2_6   = 0b0101,
      PREDIV2_7   = 0b0110,
      PREDIV2_8   = 0b0111,
      PREDIV2_9   = 0b1000,
      PREDIV2_10  = 0b1001,
      PREDIV2_11  = 0b1010,
      PREDIV2_12  = 0b1011,
      PREDIV2_13  = 0b1100,
      PREDIV2_14  = 0b1101,
      PREDIV2_15  = 0b1110,
      PREDIV2_16  = 0b1111
   }PREDIV2Factor;

   //! Bits 7:4 PREDIV2[3:0]: PREDIV2 division factor
   /*!
      Set and cleared by software to select PREDIV2 division factor. These bits can be written only
      when both PLL2 and PLL3 are disabled.
      0000: PREDIV2 input clock not divided
      0001: PREDIV2 input clock divided by 2
      0010: PREDIV2 input clock divided by 3
      0011: PREDIV2 input clock divided by 4
      0100: PREDIV2 input clock divided by 5
      0101: PREDIV2 input clock divided by 6
      0110: PREDIV2 input clock divided by 7
      0111: PREDIV2 input clock divided by 8
      1000: PREDIV2 input clock divided by 9
      1001: PREDIV2 input clock divided by 10
      1010: PREDIV2 input clock divided by 11
      1011: PREDIV2 input clock divided by 12
      1100: PREDIV2 input clock divided by 13
      1101: PREDIV2 input clock divided by 14
      1110: PREDIV2 input clock divided by 15
      1111: PREDIV2 input clock divided by 16
   */
   void configurePREDIV2( PREDIV2Factor prediv );

   typedef enum
   {
      PLL2_8      = 0b0110,
      PLL2_9      = 0b0111,
      PLL2_10     = 0b1000,
      PLL2_11     = 0b1001,
      PLL2_12     = 0b1010,
      PLL2_13     = 0b1011,
      PLL2_14     = 0b1100,
      PLL2_RSVD   = 0b1101,
      PLL2_16     = 0b1110,
      PLL2_20     = 0b1111
   }PLL2MULFactor;

   //! Bits 11:8 PLL2MUL[3:0]: PLL2 Multiplication Factor
   /*!
      Set and cleared by software to control PLL2 multiplication factor. These bits can be written
      only when PLL2 is disabled.
      00xx: Reserved
      010x: Reserved
      0110: PLL2 clock entry x 8
      0111: PLL2 clock entry x 9
      1000: PLL2 clock entry x 10
      1001: PLL2 clock entry x 11
      1010: PLL2 clock entry x 12
      1011: PLL2 clock entry x 13
      1100: PLL2 clock entry x 14
      1101: Reserved
      1110: PLL2 clock entry x 16
      1111: PLL2 clock entry x 20
   */
   void configurePLL2MUL( PLL2MULFactor factor );

   typedef enum
   {
      PLL3_8      = 0b0110,
      PLL3_9      = 0b0111,
      PLL3_10     = 0b1000,
      PLL3_11     = 0b1001,
      PLL3_12     = 0b1010,
      PLL3_13     = 0b1011,
      PLL3_14     = 0b1100,
      PLL3_RSVD   = 0b1101,
      PLL3_16     = 0b1110,
      PLL3_20     = 0b1111
   }PLL3MULFactor;

   //! Bits 15:12 PLL3MUL[3:0]: PLL3 Multiplication Factor
   /*!
      Set and cleared by software to control PLL3 multiplication factor. These bits can be written
      only when PLL3 is disabled.
      00xx: Reserved
      010x: Reserved
      0110: PLL3 clock entry x 8
      0111: PLL3 clock entry x 9
      1000: PLL3 clock entry x 10
      1001: PLL3 clock entry x 11
      1010: PLL3 clock entry x 12
      1011: PLL3 clock entry x 13
      1100: PLL3 clock entry x 14
      1101: Reserved
      1110: PLL3 clock entry x 16
      1111: PLL3 clock entry x 20
   */
   void configurePLL3MUL( PLL3MULFactor factor );

   //! Bit 16 PREDIV1SRC: PREDIV1 entry clock source
   /*!
      Set and cleared by software to select PREDIV1 clock source. This bit can be written only
      when PLL is disabled.
      0: HSE oscillator clock selected as PREDIV1 clock entry
      1: PLL2 selected as PREDIV1 clock entry
   */
   void configurePREDIV1SRC( bool hseClock = true );

   //! Bit 18 I2S3SRC: I2S3 clock source
   /*!
      Set and cleared by software to select I2S3 clock source. This bit must be valid before
      enabling I2S3 clock.
      0: System clock (SYSCLK) selected as I2S3 clock entry
      1: PLL3 VCO clock selected as I2S3 clock entry
   */
   void configureI2S2ClockSource( bool sysclk = true );

   //! Bit 17 I2S2SRC: I2S2 clock source
   /*!
      Set and cleared by software to select I2S2 clock source. This bit must be valid before
      enabling I2S2 clock.
      0: System clock (SYSCLK) selected as I2S2 clock entry
      1: PLL3 VCO clock selected as I2S2 clock entry
   */
   void configureI2S3ClockSource( bool sysclk = true );

   void setSysClockTo24();
   void setSysClockTo48();

};

#endif /* RESETCLOCKCONTROL_HPP_ */
