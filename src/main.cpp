/* Includes */
#include <stddef.h>
#include <stdio.h>
#include "stm32f10x.h"
#include "core_cmFunc.h"

#include "Registers.hpp"
#include "SysTick.hpp"
#include "SystemTimer.hpp"
#include "SystemControlBlock.hpp"
#include "InterruptController.hpp"
#include "DefaultInterruptHandler.hpp"

#include "OutputPin.hpp"
#include "InputPin.hpp"
#include "ResetClockControl.hpp"

extern unsigned long g_pfnVectors[];
unsigned long * newVectorTable;

OutputPin pinc13 = OutputPin( PortC::instance(), 13, GPIOPort::GP_PUSH_PULL, GPIOPort::SPEED_2MHz );

using namespace CortexM;

void setDefaultHandler(void)
{
   InterruptController::instance().setInterruptHandler( NonMaskableInt_IRQn, DefaultInterruptHandler::NMI_Handler );
   InterruptController::instance().setInterruptHandler( MemoryManagement_IRQn, DefaultInterruptHandler::MemManage_Handler );
   InterruptController::instance().setInterruptHandler( BusFault_IRQn, DefaultInterruptHandler::BusFault_Handler );
   InterruptController::instance().setInterruptHandler( UsageFault_IRQn, DefaultInterruptHandler::UsageFault_Handler );
   InterruptController::instance().setInterruptHandler( SVCall_IRQn, DefaultInterruptHandler::SVC_Handler );
   InterruptController::instance().setInterruptHandler( DebugMonitor_IRQn, DefaultInterruptHandler::DebugMon_Handler );
   InterruptController::instance().setInterruptHandler( PendSV_IRQn, DefaultInterruptHandler::PendSV_Handler );
   InterruptController::instance().setInterruptHandler( SysTick_IRQn, DefaultInterruptHandler::SysTick_Handler );
}

int main(void)
{
    ResetClockControl&   rcc      = ResetClockControl::instance();
    SystemTick&          sysTick  = SystemTick::instance();
    SystemControlBlock&  scb      = SystemControlBlock::instance();
    InterruptController& nvic     = InterruptController::instance();
    PortC&               portC    = PortC::instance();

    //Configure the system clock to 48MHz
    rcc.setSysClockTo48();

    //now relocate vector table to RAM
    scb.relocateVectorTable(SRAM_BASE);

    //copy the default interrupt handler address to the RAM
    setDefaultHandler();

    nvic.setInterruptHandler( SysTick_IRQn, SystemTick::interruptHandler );
    sysTick.configure(SystemCoreClock, 1000);
    sysTick.enable();

    rcc.enableAPB2Peripheral(ResetClockControl::APB2_IOPC);

    pinc13 = OutputPin( portC, 13, GPIOPort::GP_PUSH_PULL, GPIOPort::SPEED_2MHz );

    while( true )
    {
        SystemTimer::wait(500);
        pinc13.toggle();
    }
}

void _close(void)
{
}
void _lseek(void)
{

}
void _read(void)
{
}
void _write(void)
{
}