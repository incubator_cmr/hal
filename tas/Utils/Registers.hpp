#pragma once

#include "Types.hpp"


template<typename DataType>
struct Bits
{
   Bits() :
      aValue( 0 )
   {}

   Bits( DataType value ) :
      aValue( value )
   {}

   void setBits( DataType bits, unsigned char from, unsigned char to ) volatile
   {
      unsigned char nbBits = to - from + 1;
      DataType tmpValue = (1 << nbBits) - 1;
      tmpValue &= bits;
      tmpValue <<= from;

      aValue &= ~getMask( from, to );

      aValue |= tmpValue;
   }

   void setBit( unsigned char bit ) volatile
   {
      aValue &= ~getMask( bit, bit );

      aValue |= (1 << bit);
   }

   void resetBit( unsigned char bit ) volatile
   {
      aValue &= ~getMask( bit, bit );
   }

   DataType getBits( unsigned char from, unsigned char to ) volatile
   {
      if ( from > to )
      {
         //Error
         return 0;
      }

      DataType newValue = aValue;
      newValue >>= from;
      newValue &= ((1 << (to - from + 1)) - 1);
      return newValue;
   }
   DataType getValue() volatile
   {
      return aValue;
   }

   DataType getMask( unsigned char from, unsigned char to ) volatile
   {
      unsigned char nbBits = to - from + 1;
      DataType tmpValue = (1 << nbBits) - 1;
      return tmpValue << from;
   }

   Bits& operator=( const DataType other )
   {
      aValue = other;
      return *this;
   }

   Bits& operator=( const DataType& other )
   {
      aValue = other;
      return *this;
   }

   Bits& operator=( const Bits& other )
   {
      aValue = other.aValue;
      return *this;
   }

   Bits& operator<<( const DataType& other )
   {
      aValue <<= other;
      return *this;
   }

   Bits& operator>>( const DataType& other )
   {
      aValue >>= other;
      return *this;
   }

   Bits& operator<<=( const DataType& other )
   {
      aValue <<= other;
      return *this;
   }

   Bits& operator>>=( const DataType& other )
   {
      aValue >>= other;
      return *this;
   }

   volatile DataType aValue;
};

typedef Bits<uint8_t>   UByte;
typedef Bits<uint16_t>  UShort;
typedef Bits<uint32_t>  ULong;