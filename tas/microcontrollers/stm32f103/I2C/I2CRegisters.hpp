#pragma once

#include "Utils/Registers.hpp"

namespace tas
{
    namespace microcontroller
    {
        class I2c
         {
         public:
            struct I2cRegisters
            {
               UShort CR1;
               UShort RESERVED0;
               UShort CR2;
               UShort RESERVED1;
               UShort OAR1;
               UShort RESERVED2;
               UShort OAR2;
               UShort RESERVED3;
               UShort DR;
               UShort RESERVED4;
               UShort SR1;
               UShort RESERVED5;
               UShort SR2;
               UShort RESERVED6;
               UShort CCR;
               UShort RESERVED7;
               UShort TRISE;
               UShort RESERVED8;
            };

            //Two interrupt vectors for successful address/data communication and for error condition.
            //1-byte buffer with DMA capability


            //Status flags
            enum StatusFlags
            {
               TxRxMode,      // Transmitter/Receiver mode flag
               EoBTx,         // End-of-Byte transmission flag
               I2cBusy        // I2C busy flag
            };

            enum ErrorFlags
            {
               ArbitrationLost,  // Arbitration lost condition for master mode     
               AckFailure,       // Acknowledgment failure after address/ data transmission
               DetectOfMisplace, // Detection of misplaced start or stop condition
               OverOrUnderrun    // Overrun/Underrun if clock stretching is disabled
            };

            //by default in slave mode
            //this interface automatically switches from slave to master, after it generates a START 
            //condition and from Master to slave, if an arbitration loss or a STOP generation occurs, 
            //allowing multimaster capability.
            enum ModeSelection
            {
               SlaveTransmitter,
               SlaveReceiver,
               MasterTransmitter,
               MasterReceiver
            };

            //Configurable PEC (packet error checking) generation or verification

            //Enable or disable the general call address??
            //make sure that this interface has an attribute for hardware address so that this address will be set to the hardware in slave mode

            //Data and addresses are transferred as 8-bit bytes, MSB first. The first byte following the start condition contain the address (one in 7-bit mode, two in 10-bit mode).
            //The address is always transmitted in Master mode.

            enum CR1_Bits
            {
               PE = 0,//!< Peripheral enable
               SMBUS = 1,//!< SMBus mode
               RES1 = 2,//!<
               SMBTYPE = 3,//!< SMBus type
               ENARP = 4,//!< ARP enable
               ENPEC = 5,//!< PEC enable
               ENGC,//!< General call enable
               NOSTRETCH,//!< Clock stretching disable (Slave mode)
               START,//!< Start generation
               STOP,//!< Stop generation
               ACK,//!< Acknowledge enable
               POS,//!< Acknowledge/PEC Position (for data reception)
               PEC,//!< Packet error checking
               ALERT, //!< SMBus alert
               RES2, //!<
               SWRST //!< Software reset
            };

            typedef union
            {
            struct
            {
               unsigned long pe      : 1;
               unsigned long smbus    : 1;
               unsigned long res1    : 1;
               unsigned long smbtype    : 1;
               unsigned long enARP : 1;
               unsigned long enPEC : 1;
               unsigned long enGC : 1;
               unsigned long noStretch : 1;
               unsigned long start : 1;
               unsigned long stop : 1;
               unsigned long ack : 1;
               unsigned long pos : 1;
               unsigned long pec : 1;
               unsigned long alert : 1;
               unsigned long res2 : 1;
               unsigned long swrst : 1;
            } bits;
            unsigned short data;
            } ControlRegister1;

            enum CR2_Bits
            {
               FREQ = 0, //!< Peripheral clock frequency start
               FREQ_SIZE = 6, //!< Peripheral clock frequency end
               ITERREN = 8, //!< Error interrupt enable
               ITEVTEN = 9, //!< Event interrupt enable
               ITBUFEN = 10, //!< Buffer interrupt enable
               DMAEN = 11, //!< DMA requests enable
               LAST = 12 //!< DMA last transfer
            };

            enum EnableControlBits
            {
               eItEvfEn,
               eItBufEn,
               eItErrEn
            };

            typedef union
            {
            struct
            {
               unsigned long freq      : 6;
               unsigned long rsvd1    : 2;
               unsigned long itErrEn    : 1;
               unsigned long itEvtEn    : 1;
               unsigned long itBufEn : 1;
               unsigned long dmaEn : 1;
               unsigned long last : 1;
               unsigned long rsvd2 : 3;
            } bits;
            unsigned short data;
            } ControlRegister2;

            enum OAR1_Bits
            {
               ADD0 = 0,
               ADD = 1,
               ADD_SIZE = 7,
               ADD1 = 8,
               ADD1_SIZE = 2,
               ADDMODE = 15
            };

            typedef union
            {
            struct
            {
               unsigned long add0    : 1;
               unsigned long addr    : 7;
               unsigned long add1    : 1;
               unsigned long rsvd    : 5;
               unsigned long addMode : 1;
            } bits;
            unsigned short data;
            } OwnAddressRegister1;

            enum OAR2_Bits
            {
               ENDUAL = 0,
               ADD2 = 1,
               ADD2_SIZE = 7
            };

            typedef union
            {
            struct
            {
               unsigned long enDual    : 1;
               unsigned long add2    : 7;
               unsigned long rsvd    : 8;
            } bits;
            unsigned short data;
            } OwnAddressRegister2;

            enum DR_Bits
            {
               DR = 0,
               DRSize = 8
            };

            typedef union
            {
            struct
            {
               unsigned long dr    : 8;
               unsigned long rsvd  : 8;
            } bits;
            unsigned short data;
            } DataRegister;

            enum SR1_Bits
            {
               SB = 0,
               ADDR = 1,
               BTF = 2,
               ADD10 = 3,
               STOPF = 4,
               RxNE = 6,
               TxNE = 7,
               BERR = 8,
               ARLO = 9,
               AF = 10,
               OVR = 11,
               PECERR = 12,
               TIMEOUT = 14,
               SMBALERT = 15
            };

            typedef union
            {
            struct
            {
               unsigned long sb      : 1;
               unsigned long addr    : 1;
               unsigned long btf    : 1;
               unsigned long add10    : 1;
               unsigned long stopf : 1;
               unsigned long res1 : 1;
               unsigned long rxNE : 1;
               unsigned long txNE : 1;
               unsigned long bErr : 1;
               unsigned long arlo : 1;
               unsigned long af : 1;
               unsigned long ovr : 1;
               unsigned long pecErr : 1;
               unsigned long res2 : 1;
               unsigned long timeout : 1;
               unsigned long smbAlert : 1;
            } bits;
            unsigned short data;
            } StatusRegister1;

            enum SR2_Bits
            {
               MSL = 0,
               BUSY = 1,
               TRA = 2,
               GENCALL = 3,
               SMBDEFAULT = 4,
               SMBHOST = 6,
               DUALF = 7,
               PECh = 8,
               PECSize = 8
            };

            typedef union
            {
            struct
            {
               unsigned long msl      : 1;
               unsigned long busy    : 1;
               unsigned long tra    : 1;
               unsigned long res    : 1;
               unsigned long genCall : 1;
               unsigned long smbDefault : 1;
               unsigned long smbHost : 1;
               unsigned long dualf : 1;
               unsigned long pec : 8;
            } bits;
            unsigned short data;
            } StatusRegister2;

            enum CCR_Bits
            {
               CCR = 0,
               CCRSize = 12,
               DUTY = 14,
               FS = 15
            };

            typedef union
            {
            struct
            {
               unsigned long ccr      : 12;
               unsigned long res    : 2;
               unsigned long duty    : 1;
               unsigned long fs    : 1;
            } bits;
            unsigned short data;
            } ClockControlRegister;

            void reset( void );
            void enable( void );
            void disable( void );
            void generateStartCondition( void );
            void generateStopCondition( void );
            void clearStop( void );
            void setOwn7BitsSlaveAddress( uint8_t address );
            void setOwn10BitsSlaveAddress( uint16_t address );
            void enableDualAddressingMode( void );
            void disableDualAddressingMode( void );
            void setClockFrequency( uint8_t frequency );
            void sendData( uint8_t data );
            void setFastMode( void );
            void setStandardMode( void );
            void send7BitsAddress( uint8_t address, uint8_t rw = 0 );
            uint8_t getData( void );
            void enableInterrupt( EnableControlBits id );
            void disableInterrupt( EnableControlBits id );
            void enableAck( void );
            void disableAck( void );
            void nackNext( void );
            void nackCurrent( void );
            void setdutyCycle( bool div2 = true );
            void enableDMA( void );
            void disableDMA( void );
            void setDMALastTransfer( void );
            void clearDMALastTransfer( void );
            void setSpeed( void );


         protected:
            I2c( uint32_t addressMap )
            {
                  registers = reinterpret_cast<I2cRegisters*>( addressMap );
            }
            volatile I2cRegisters* registers;

         };

    }
}
