
#include "I2CRegisters.hpp"

using tas::microcontroller::I2c;

void I2c::reset( void )
{
   registers->CR1.setBit(SWRST);
}

void I2c::enable( void )
{
   registers->CR1.setBit(PE);
}

void I2c::disable( void )
{
   registers->CR1.resetBit(PE);
}

void I2c::generateStartCondition( void )
{
   registers->CR1.setBit(START);
}

void I2c::generateStopCondition( void )
{
   registers->CR1.setBit(STOP);
}

void I2c::clearStop( void )
{
   registers->CR1.resetBit(STOP);
}

void I2c::setOwn7BitsSlaveAddress( uint8_t address )
{
   registers->OAR1.setBits(address, ADD, (ADD_SIZE + ADD - 1) );
}

void I2c::setOwn10BitsSlaveAddress( uint16_t address )
{

}

void I2c::enableDualAddressingMode( void )
{
   registers->OAR2.setBit(ENDUAL);
}

void I2c::disableDualAddressingMode( void )
{
   registers->OAR2.resetBit(ENDUAL);
}

void I2c::setClockFrequency( uint8_t frequency )
{
   if ( frequency < 2 || frequency > 50 )
   {
      return ;
   }

   registers->CR2.setBits( frequency, CCR, ( CCR + CCRSize -1 ) );
}

void I2c::sendData( uint8_t data )
{
   registers->DR.setBits( data, DR, (DR + DRSize - 1 ) );
}

void I2c::setFastMode( void )
{
   registers->CCR.setBit(FS);
}

void I2c::setStandardMode( void )
{
   registers->CCR.resetBit(FS);
}

void I2c::send7BitsAddress( uint8_t address, uint8_t rw  )
{
   registers->DR.aValue = (uint8_t)( (address << 1) | rw ) ;
}

uint8_t I2c::getData( void )
{
   return registers->DR.aValue && 0xFF;
}

void I2c::enableInterrupt( EnableControlBits id )
{
   switch ( id )
   {
   case eItEvfEn:
   {
      registers->CR2.setBit(ITEVTEN);
      break;
   }

   case eItBufEn:
   {
      registers->CR2.setBit(ITBUFEN);
      break;
   }

   case eItErrEn:
   {
      registers->CR2.setBit(ITERREN);
      break;
   }
   default:
      return ;
   }

}

void I2c::disableInterrupt( EnableControlBits id )
{
   switch ( id )
   {
   case eItEvfEn:
   {
      registers->CR2.resetBit(ITEVTEN);
      break;
   }

   case eItBufEn:
   {
      registers->CR2.resetBit(ITBUFEN);
      break;
   }

   case eItErrEn:
   {
      registers->CR2.resetBit(ITERREN);
      break;
   }
   default:
      return ;
   }
}

void I2c::enableAck( void )
{
   registers->CR1.setBit(ACK);
}

void I2c::disableAck( void )
{
   registers->CR1.resetBit(ACK);
}

void I2c::nackNext( void )
{
   registers->CR1.setBit(POS);
}

void I2c::nackCurrent( void )
{
   registers->CR1.resetBit(POS);
}

void I2c::setdutyCycle( bool div2 )
{
   if ( div2 )
   {
      registers->CCR.resetBit(DUTY);
   }
   else
   {
      registers->CCR.setBit(DUTY);
   }
}

void I2c::enableDMA( void )
{
   registers->CR2.setBit(DMAEN);
}

void I2c::disableDMA( void )
{
   registers->CR2.resetBit(DMAEN);
}

void I2c::setDMALastTransfer( void )
{
   registers->CR2.setBit(LAST);
}

void I2c::clearDMALastTransfer( void )
{
   registers->CR2.resetBit(LAST);
}

void I2c::setSpeed( void )
{

}






