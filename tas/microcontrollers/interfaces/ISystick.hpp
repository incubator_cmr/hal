#pragma once

#include "Types.hpp"

namespace tas
{
    namespace microcontroller
    {
        class ISystick
        {
            /**
             * @brief Enable or disable the Systick
             * 
             * @param enable true if the systick should be enabled and false if it should be disabled
             */ 
            virtual void enable( const bool enable ) = 0;

            /***
             * @brief Check whether the Systick peripheral is enabled
             * 
             * @return true if the systick is enabled otherwise false
             */ 
            virtual bool isEnable( void ) = 0;

            /**
             * @brief Configure the Systick peripheral
             * 
             * @param inputClkFreq  The current input clock frequency
             * @param tick          tick to use (1000) for 1ms resolution
             * @param useCoreClk    flag the use of the core clock. Should be set to true
             */
            virtual void configure( const unsigned long inputClkFreq, 
                                    const unsigned long tick, 
                                    const bool useCoreClk ) = 0;
            /**
             * @brief Get the current tick
             * 
             * @return the current tick
             */
            virtual uint32_t GetCurrentTick() = 0;

        }
    }
}
